var keys; 
function renderTable(string)
{
    if (string.status == '404')
                        {
                            $("#table1").find('table').remove();
                            alert("this Table has no data to show");
                            
                        }
                    else
                        {
                            keys =Object.keys(string.data[0]);
                            $("#table1").find('table').remove();
                            var str = '<table class=\"table\"><tr>';
                            for(i=0; i<keys.length; i++)
                             str+= "<th>"+keys[i]+"</th>";
                            str+="</tr>";
                        for(k=0; k<string.data.length; k++)
                        { 
                            str+= "<tr>";
                            for (i=0; i<keys.length;i++)
                            {
                                str+= "<td>"+string.data[k][keys[i]]+"</td>";
                            }
                            str+="</tr>";
                        }
                            
                            str+="</table>";  
                            //console.log(str);
                            $("#table1").append(str);
                        }
}

$(document).ready(function(){
    $("#form").submit(function(e){
    e.preventDefault();
    var dataString = $("#form").serializeArray();
    //console.log(dataString[0].value);    
        $.ajax({
                type: "POST",
                url: "../app/showdb.php",
                data: dataString,
                beforeSend: function(){
                    $("#show").val('Loading');
                    
                },
                success: function(result)
                {
                    //console.log(result);
                    var string = JSON.parse(result);
                    localStorage.setItem('host',dataString[0].value);
                    localStorage.setItem('port',dataString[1].value);
                    localStorage.setItem('uname',dataString[2].value);
                    localStorage.setItem('pass',dataString[3].value);
                    
                    $("#content").find('button').remove();
                    //console.log(string[0].TABLE_SCHEMA);
                    for (i=0; i< string.length; i++ )
                        {
                            var data= string[i].TABLE_SCHEMA;
                            var str= "<option value=\""+data+"\" >"+data+"</option>";
                            $('select').append(str);
                            
                        }
                    
                    var str = "<button type=\"button\ id=\"table\">Click Me!</button>";
                    $("#content").append(str);
                },
                complete: function(){
                     $("#show").val('Dbs loaded');
                    
                }
                }); 
    });
    $("#content").on('click','button', function() {
                
        var dbname = $("#users").val();
        $.ajax({
                type: "POST",
                url: "../app/showtable.php",
                data: {
                    hname: localStorage.getItem('host'),
                    uname:localStorage.getItem('uname'),
                    pass: localStorage.getItem('pass'),
                    dbname: dbname
                },
                success: function(result)
                {
                    var string = JSON.parse(result);
                    localStorage.setItem('dbname',dbname);
                    $("#table").find('select').remove();
                    $("#table").find('button').remove();
                    
                    
                    var str = "<select name =\"tables\" id=\"selecttables\">";
                    for (i=0; i< string.length; i++ )
                        {
                            var data= string[i].TABLE_NAME;
                             str+= "<option value=\""+data+"\" >"+data+"</option>";
                        }
                    str+="</select>";
                    $("#table").append(str);
                    
                    var str = '<button class="data">Show Table Records</button>';
                    $("#table").append(str);
                }   
                }); 
    });
    
$("#table").on('click','.data',function(){
    
   var tname =$("#table").find('select').val();
   localStorage.setItem('tname',tname);
    $("#table").find('form').remove();
    $("#table").find('#col').remove();
    $.ajax({
                type: "POST",
                url: "../app/showtabledata.php",
                data: {
                    hname: localStorage.getItem('host'),
                    uname:localStorage.getItem('uname'),
                    pass: localStorage.getItem('pass'),
                    tname: tname,
                    dbname:localStorage.getItem('dbname')
                },
                success: function(result)
                {
                   var string = JSON.parse(result);
                    renderTable(string);
                    var str = "<form id=\"columns\">";
                            for(i=0; i<keys.length; i++)
                                str+="<input type=\"checkbox\" name=\"columns\" value="+keys[i]+">"+keys[i]+"";
                            str+='</form><button type="button" id="col" >Select Columns</button>';
                            $("#table").append(str);
                }      
                });
        });
    
   $("#table").on('click','#col',function(){
       
    var dataString =   $("#table").find('form').serializeArray();
        $.ajax({
                type: "POST",
                url: "../app/showColData.php",
                data: {
                    hname: localStorage.getItem('host'),
                    uname:localStorage.getItem('uname'),
                    pass: localStorage.getItem('pass'),
                    tname: localStorage.getItem('tname'),
                    dbname:localStorage.getItem('dbname'),
                    cols  :dataString
                },
                success: function(result)
                {
                    var string = JSON.parse(result);
                     renderTable(string);   
                            var options = '<form id="queryform">';
                            for(i=0; i<keys.length; i++)
                            {    
                                options+="<label>"+keys[i]+"</label>";
                                options+='<select name="query"><option value="=">=</option><option value=">">></option><option value="<"><</option></select>';
                                options+='<input type="text" name="'+keys[i]+'">';
                            }
                            options+='</form><button type="button" id="sendquery" >Show New Data</button>';
                            
                            
                            $("#table").append(options);
                }
                
        });
   }); 
    
    $("#table").on('click','#sendquery',function(){
        
       var dataString = $("#table").find('#queryform').serializeArray();
        
          $.ajax({
                type: "POST",
                url: "../app/showData.php",
                data: {
                    hname: localStorage.getItem('host'),
                    uname:localStorage.getItem('uname'),
                    pass: localStorage.getItem('pass'),
                    tname: localStorage.getItem('tname'),
                    dbname:localStorage.getItem('dbname'),
                    query: dataString
                },
                success: function(result)
                {   
                    console.log(result);
                    var string = JSON.parse(result);
                    renderTable(string);
            }
        });
    });
});