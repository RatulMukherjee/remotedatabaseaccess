$(document).ready(function(){
        $("#form").submit(function(e){
    e.preventDefault();
    var dataString = $("#form").serializeArray();
    //console.log(dataString[0].value);    
        $.ajax({
                type: "POST",
                url: "../app/showdb.php",
                data: dataString,
                beforeSend: function(){
                    $("#show").val('Loading');
                    
                },
                success: function(result)
                {
                    //console.log(result);
                    var string = JSON.parse(result);
                    localStorage.setItem('host',dataString[0].value);
                    localStorage.setItem('port',dataString[1].value);
                    localStorage.setItem('uname',dataString[2].value);
                    localStorage.setItem('pass',dataString[3].value);
                    
                    $("#content").find('button').remove();
                    //console.log(string[0].TABLE_SCHEMA);
                    for (i=0; i< string.length; i++ )
                        {
                            var data= string[i].TABLE_SCHEMA;
                            var str= "<option value=\""+data+"\" >"+data+"</option>";
                            $('select').append(str);
                            
                        }
                    
                    var str = '<button type="button" id="all">Generate All!</button>';
                     str+= '<button type="button" id="entity">Select Table and generate</button>';
                    $("#content").append(str);
                },
                complete: function(){
                     $("#show").val('Dbs loaded');
                }
                }); 
            
           $("#content").on('click','#all',function(){
                 $.ajax({
                        type: "POST",
                        url: "../app/genAllEntity.php",
                        data: {
                            hname: localStorage.getItem('host'),
                            uname:localStorage.getItem('uname'),
                            pass: localStorage.getItem('pass'),
                            dbname: $("#users").val()
                           
                        },
                        success: function(result)
                        {
                            //var string = JSON.parse(result);
                            console.log(result);
                        }   
                        }); 
             
               
           }); 
            $("#content").on('click','#entity',function(){
              //console.log("generate single"); 
                var dbname = $("#users").val();
                $.ajax({
                        type: "POST",
                        url: "../app/showtable.php",
                        data: {
                            hname: localStorage.getItem('host'),
                            uname:localStorage.getItem('uname'),
                            pass: localStorage.getItem('pass'),
                            dbname: dbname
                        },
                        success: function(result)
                        {
                            var string = JSON.parse(result);
                            localStorage.setItem('dbname',dbname);
                            $("#table").find('select').remove();
                            $("#table").find('button').remove();


                            var str = "<select name =\"tables\" id=\"selecttables\">";
                            for (i=0; i< string.length; i++ )
                                {
                                    var data= string[i].TABLE_NAME;
                                     str+= "<option value=\""+data+"\" >"+data+"</option>";
                                }
                            str+="</select>";
                            $("#table").append(str);

                            var str = '<button class="data" id="genentity">Select table and generate entity</button>';
                            $("#table").append(str);
                        }   
                        }); 
           });
        $("#table").on('click','#genentity', function(){
            //console.log($("#table").find('select').val());
                $.ajax({
                        type: "POST",
                        url: "../app/genSingleEntity.php",
                        data: {
                            hname: localStorage.getItem('host'),
                            uname:localStorage.getItem('uname'),
                            pass: localStorage.getItem('pass'),
                            dbname: localStorage.getItem('dbname'),
                            tname: $("#table").find('select').val()
                        },
                        success: function(result)
                        {
                            //var string = JSON.parse(result);
                            console.log(result);
                        }   
                        }); 
        }); 
    });
});