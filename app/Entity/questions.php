<?php
class questions {
	var $qid;
	var $question;
	var $option1;
	var $option2;
	var $option3;
	var $option4;
	var $answer;

	public function setQid($qid) {
		$this->qid=$qid;
	}
	public function getQid($qid) {
		return $this->qid;
	}

	public function setQuestion($question) {
		$this->question=$question;
	}
	public function getQuestion($question) {
		return $this->question;
	}

	public function setOption1($option1) {
		$this->option1=$option1;
	}
	public function getOption1($option1) {
		return $this->option1;
	}

	public function setOption2($option2) {
		$this->option2=$option2;
	}
	public function getOption2($option2) {
		return $this->option2;
	}

	public function setOption3($option3) {
		$this->option3=$option3;
	}
	public function getOption3($option3) {
		return $this->option3;
	}

	public function setOption4($option4) {
		$this->option4=$option4;
	}
	public function getOption4($option4) {
		return $this->option4;
	}

	public function setAnswer($answer) {
		$this->answer=$answer;
	}
	public function getAnswer($answer) {
		return $this->answer;
	}
}
?>