<?php
class game_requests {
	var $sender_id;
	var $receiver_id;
	var $sender_name;
	var $receiver_name;
	var $box1;
	var $box2;
	var $box3;
	var $box4;
	var $box5;
	var $box6;
	var $box7;
	var $box8;
	var $box9;
	var $turn;
	var $result;

	public function setSender_id($sender_id) {
		$this->sender_id=$sender_id;
	}
	public function getSender_id($sender_id) {
		return $this->sender_id;
	}

	public function setReceiver_id($receiver_id) {
		$this->receiver_id=$receiver_id;
	}
	public function getReceiver_id($receiver_id) {
		return $this->receiver_id;
	}

	public function setSender_name($sender_name) {
		$this->sender_name=$sender_name;
	}
	public function getSender_name($sender_name) {
		return $this->sender_name;
	}

	public function setReceiver_name($receiver_name) {
		$this->receiver_name=$receiver_name;
	}
	public function getReceiver_name($receiver_name) {
		return $this->receiver_name;
	}

	public function setBox1($box1) {
		$this->box1=$box1;
	}
	public function getBox1($box1) {
		return $this->box1;
	}

	public function setBox2($box2) {
		$this->box2=$box2;
	}
	public function getBox2($box2) {
		return $this->box2;
	}

	public function setBox3($box3) {
		$this->box3=$box3;
	}
	public function getBox3($box3) {
		return $this->box3;
	}

	public function setBox4($box4) {
		$this->box4=$box4;
	}
	public function getBox4($box4) {
		return $this->box4;
	}

	public function setBox5($box5) {
		$this->box5=$box5;
	}
	public function getBox5($box5) {
		return $this->box5;
	}

	public function setBox6($box6) {
		$this->box6=$box6;
	}
	public function getBox6($box6) {
		return $this->box6;
	}

	public function setBox7($box7) {
		$this->box7=$box7;
	}
	public function getBox7($box7) {
		return $this->box7;
	}

	public function setBox8($box8) {
		$this->box8=$box8;
	}
	public function getBox8($box8) {
		return $this->box8;
	}

	public function setBox9($box9) {
		$this->box9=$box9;
	}
	public function getBox9($box9) {
		return $this->box9;
	}

	public function setTurn($turn) {
		$this->turn=$turn;
	}
	public function getTurn($turn) {
		return $this->turn;
	}

	public function setResult($result) {
		$this->result=$result;
	}
	public function getResult($result) {
		return $this->result;
	}
}
?>