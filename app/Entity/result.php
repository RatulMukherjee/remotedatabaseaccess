<?php
class result {
	var $rid;
	var $eid;
	var $start_time;
	var $end_time;
	var $marks;

	public function setRid($rid) {
		$this->rid=$rid;
	}
	public function getRid($rid) {
		return $this->rid;
	}

	public function setEid($eid) {
		$this->eid=$eid;
	}
	public function getEid($eid) {
		return $this->eid;
	}

	public function setStart_time($start_time) {
		$this->start_time=$start_time;
	}
	public function getStart_time($start_time) {
		return $this->start_time;
	}

	public function setEnd_time($end_time) {
		$this->end_time=$end_time;
	}
	public function getEnd_time($end_time) {
		return $this->end_time;
	}

	public function setMarks($marks) {
		$this->marks=$marks;
	}
	public function getMarks($marks) {
		return $this->marks;
	}
}
?>